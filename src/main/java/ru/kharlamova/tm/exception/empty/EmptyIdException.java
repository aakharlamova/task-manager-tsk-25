package ru.kharlamova.tm.exception.empty;

import ru.kharlamova.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty.");
    }

}
